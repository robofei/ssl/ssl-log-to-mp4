# SSL Log to MP4 Converter

Tool that converts an SSL log (.log or .gz) to an MP4 video

## Dependencies

- Python 3.8.5+
  - gizeh
  - moviepy
  - numpy

The dependencies can be install using the following command:

```shell
sudo apt install python-pip ffmpeg libffi-dev
pip3 install gizeh moviepy numpy
```

The pip3 command can also be replaced by:

```shell
python3.8 -m pip install gizeh moviepy numpy
```

## Usage

The tool can be run with the following command:

```shell
python3.8 src/LogConverter.py -f path_to_log_file.log.gz
```

or, if you have python3.8 in you /usr/bin:

```shell
./src/LogConverter.py -f path_to_log_file.log.gz
```

The video will be saved in the same folder as the log file, an example
of the output of this tool can be seen below.

[Video Output](./etc/example.mp4)

The video frame rate can also be changed using the `-v` or `--video-fps`
argument.

By the default, stuff that happens during the **STOP** and **HALT** commands
will be ignored.

**NOTE**: The tool also accepts decompressed files, i.e. `.log`, and should
detect that automatically using the file extension

The `create_log_videos.sh` can also be used to create the videos for many log
files in just one batch, the script reads the file names from `log_list.txt`.
Note that the full path must be provided, or, a relative path (with `../`).
This is not the prettiest solution and it should be updated soon.
